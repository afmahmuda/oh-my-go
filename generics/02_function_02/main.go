package main

import "fmt"

type (
	response struct {
		Result *string `json:"result,omitempty"`
		Value  *int    `json:"value,omitempty"`
	}
)

func (r response) String() string {
	return fmt.Sprintf("Res: `%v`, Val : `%v`", safePrintPtr(r.Result), safePrintPtr(r.Value))
}

func main() {
	response := response{}
	fmt.Println(response)

	response.Result = toPtr("ehehe")
	fmt.Println(response)

	response.Value = toPtr(98)
	fmt.Println(response)

}

func toPtr[T any](val T) *T {
	return &val
}

func safePrintPtr[T any](ptr *T) T {
	if ptr == nil {
		v := new(T)
		return *v

	}
	return *ptr
}
