package main

import (
	"encoding/json"
	"fmt"
)

func main() {

	res1, err := unmarshal[response]([]byte(`{"result":"hehe", "value": 10, "error":"none", "message":"no error"}`))
	if err != nil {
		fmt.Println("err 1\t:", err)
	} else {

		fmt.Printf("res 1\t: %+[1]v\n", res1)
	}

	res2, err := unmarshal[success]([]byte(`{"result":"hehe", "value": 10, "error":"none", "message":"no error"}`))
	if err != nil {
		fmt.Println("err 2\t:", err)
	} else {

		fmt.Printf("res 2\t: %+[1]v\n", res2)
	}

	res3, err := unmarshal[failure]([]byte(`{"result":"hehe", "value": 10, "error":"none", "message":"no error"}`))
	if err != nil {
		fmt.Println("err 3\t:", err)
	} else {

		fmt.Printf("res 3\t: %+[1]v\n", res3)
	}

	res4, err := unmarshal[invalidSuccess]([]byte(`{"result":"hehe", "value": 10, "error":"none", "message":"no error"}`))
	if err != nil {
		fmt.Println("err 4\t:", err)
	} else {

		fmt.Printf("res 4\t: %+[1]v\n", res4)
	}

	res5, err := unmarshal[kentang]([]byte(`{"result":"hehe", "value": 10, "error":"none", "message":"no error"}`))
	if err != nil {
		fmt.Println("err 5\t:", err)
	} else {

		fmt.Printf("res 5\t: %+[1]v\n", res5)
	}

	res6, err := unmarshal[struct{}]([]byte(`{"result":"hehe", "value": 10, "error":"none", "message":"no error"}`))
	if err != nil {
		fmt.Println("err 6\t:", err)
	} else {

		fmt.Printf("res 6\t: %+[1]v\n", res6)
	}

	res7, err := unmarshal[struct {
		Value   int    `json:"value"`
		Error   string `json:"error"`
	}]([]byte(`{"result":"hehe", "value": 10, "error":"none", "message":"no error"}`))
	if err != nil {
		fmt.Println("err 7\t:", err)
	} else {

		fmt.Printf("res 7\t: %+[1]v\n", res7)
	}

}

type (
	response struct {
		Result  string `json:"result,omitempty"`
		Value   int    `json:"value,omitempty"`
		Error   string `json:"error,omitempty"`
		Message string `json:"message,omitempty"`
	}

	success struct {
		Result string `json:"result,omitempty"`
		Value  int    `json:"value,omitempty"`
	}

	failure struct {
		Error   string `json:"error,omitempty"`
		Message string `json:"message,omitempty"`
	}

	kentang string

	invalidSuccess struct {
		Result string `json:"result,omitempty"`
		Value  string `json:"value,omitempty"`
	}
)

func unmarshal[T any](source []byte) (T, error) {

	result := new(T)

	if err := json.Unmarshal(source, result); err != nil {
		return *result, err
	}

	return *result, nil
}
