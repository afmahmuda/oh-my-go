package main

import (
	"fmt"
)

func main() {

	println(bayi[bulan, int]{
		nama: "rizky",
		umur: bulan{value: 10},
	}.String(),
	)
	println(bayi[tahun[int], int]{
		nama: "anda",
		umur: tahun[int]{value: 10},
	}.String(),
	)

	println(bayi[tahun[float64], float64]{
		nama: "dodo",
		umur: tahun[float64]{value: 2.65},
	}.String(),
	)
}

type (
	bulan struct {
		value int
	}

	tahun[T int | float64] struct {
		value T
	}

	umur[T int | float64] interface {
		bulan | tahun[T]
		String() string
	}

	bayi[U umur[T], T int | float64] struct {
		nama string
		umur U
	}
)

func (b bulan) String() string {
	return fmt.Sprintf("%v bulan", b.value)
}

func (t tahun[T]) String() string {
	return fmt.Sprintf("%v tahun", t.value)
}

func (b bayi[U, T]) String() string {
	return fmt.Sprintf("baby %s , %v ", b.nama, b.umur.String())
}
