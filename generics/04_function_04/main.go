package main

import "fmt"

func main() {

	ints := []int{1, 2, 34}
	strings := []string{"apple", "pie", "cola"}
	type simpleStruct struct {
		name string
		age  int
	}

	simpleStructs := []simpleStruct{
		{name: "paklik", age: 40},
		{name: "bapak", age: 42},
		{name: "adik", age: 8},
	}

	fmt.Println(`Filter(ints, func(got int) bool { return got < 1 }) |>>`,"\t",Filter(ints, func(got int) bool { return got < 1 }))
	fmt.Println(`Filter(ints, func(got int) bool { return got >= 2 }) |>>`,"\t",Filter(ints, func(got int) bool { return got >= 2 }))
	fmt.Println(`Filter(ints, func(got int) bool { return got == 34 }) |>>`,"\t",Filter(ints, func(got int) bool { return got == 34 }))
	fmt.Println(`Filter(strings, func(got string) bool { return len(got) > 3 }) |>>`,"\t",Filter(strings, func(got string) bool { return len(got) > 3 }))
	fmt.Println(`Filter(simpleStructs, func(got simpleStruct) bool { return got.age > 10 }) |>>`,"\t",Filter(simpleStructs, func(got simpleStruct) bool { return got.age > 10 }))

	fmt.Println(`IsIn(ints, 0) |>>`,"\t",IsIn(ints, 0))
	fmt.Println(`IsIn(ints, 34) |>>`,"\t",IsIn(ints, 34))
	fmt.Println(`IsIn(strings, "pie") |>>`,"\t",IsIn(strings, "pie"))
	fmt.Println(`IsIn(strings, "pies") |>>`,"\t",IsIn(strings, "pies"))
	fmt.Println(`IsIn(simpleStructs, simpleStruct{name: "paklik", age: 40}) |>>`,"\t",IsIn(simpleStructs, simpleStruct{name: "paklik", age: 40}))
	fmt.Println(`IsIn(simpleStructs, simpleStruct{name: "buklik", age: 36}) |>>`,"\t",IsIn(simpleStructs, simpleStruct{name: "buklik", age: 36}))

	var aNilStrings []string
	fmt.Println(`ToPtr("mamamia") |>>`,"\t",ToPtr("mamamia"))
	fmt.Println(`ToPtr(19) |>>`,"\t",ToPtr(19))
	fmt.Println(`ToPtr(aNilStrings) |>>`,"\t",ToPtr(aNilStrings))

	var aNilInt *int
	var aNilString *string
	fmt.Println(`ValueOrDefault(ToPtr("mamamia")) |>>`,"\t",ValueOrDefault(ToPtr("mamamia")))
	fmt.Println(`ValueOrDefault(ToPtr(129)) |>>`,"\t",ValueOrDefault(ToPtr(129)))
	fmt.Println(`ValueOrDefault(aNilString) |>>`,"\t",ValueOrDefault(aNilString))
	fmt.Println(`ValueOrDefault(aNilInt) |>>`,"\t",ValueOrDefault(aNilInt))

}

func IsIn[T comparable](slice []T, target T) bool {
	for _, v := range slice {
		if v == target {
			return true
		}
	}
	return false
}

func Filter[T any](slice []T, searchFunc func(T) bool) []T {

	result := []T{}
	for _, v := range slice {
		if searchFunc(v) {
			result = append(result, v)
		}
	}
	return result
}

func ToPtr[T any](value T) *T {
	return &value
}

func ValueOrDefault[T any](prt *T) T {
	v := new(T)

	if prt != nil {
		*v = *prt
	}

	return *v
}
