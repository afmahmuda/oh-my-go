package main

import "fmt"

func main() {

	println("===")

	fmt.Printf("addAbsoluteInt \t : (%[1]T)%[1]v\n", addAbsoluteInt(10, -99))
	fmt.Printf("addAbsoluteFloat \t : (%[1]T)%[1]v\n", addAbsoluteFloat(10, -99))
	println("===")

	fmt.Printf("addAbsoluteSimple \t : (%[1]T)%[1]v\n", addAbsoluteSimple(10, -99))
	fmt.Printf("addAbsoluteSimple \t : (%[1]T)%[1]v\n", addAbsoluteSimple(10.1, -98.9))
	println("===")

	fmt.Printf("addAbsoluteConstraint \t : (%[1]T)%[1]v\n", addAbsoluteConstraint(10.0, -99.0))
	println("===")

	fmt.Printf("addAbsoluteConstraintOwnFloat \t : (%[1]T)%[1]v\n", addAbsoluteConstraintOwnFloat(10.0, float(-99.0)))
	println("===")

	fmt.Printf("addAbsoluteFromWhateverAsFirst \t : (%[1]T)%[1]v\n", addAbsoluteFromWhateverAsFirst(10, -99))
	fmt.Printf("addAbsoluteFromWhateverAsFirst \t : (%[1]T)%[1]v\n", addAbsoluteFromWhateverAsFirst(10, -99.4))
	fmt.Printf("addAbsoluteFromWhateverAsSecond \t : (%[1]T)%[1]v\n", addAbsoluteFromWhateverAsSecond(10, -99.4))
	fmt.Printf("addAbsoluteFromWhateverAsSecond \t : (%[1]T)%[1]v\n", addAbsoluteFromWhateverAsSecond(10, -99.4))
	println("===")

	fmt.Printf("addAbsoluteFromWhateverAsWhatever \t : (%[1]T)%[1]v\n", addAbsoluteFromWhateverAsWhatever[int, float64, float64](10, -99))
	fmt.Printf("addAbsoluteFromWhateverAsWhatever \t : (%[1]T)%[1]v\n", addAbsoluteFromWhateverAsWhatever[int, float64, int](10, -99))
	fmt.Printf("addAbsoluteFromWhateverAsWhatever \t : (%[1]T)%[1]v\n", addAbsoluteFromWhateverAsWhatever[float64, float64, float64](10.5, -98.5))
	println("===")

}

//======================= no generics ======

func absoluteFloat(val float64) float64 {
	if val < 0 {
		return -val
	}
	return val
}

func addAbsoluteFloat(a, b float64) float64 {
	return absoluteFloat(a) + absoluteFloat(b)
}

func absoluteInt(val int) int {
	if val < 0 {
		return -val
	}
	return val
}

func addAbsoluteInt(a, b int) int {
	return absoluteInt(a) + absoluteInt(b)
}

//=======================

//=======================  generics 1 : simple ======

func absoluteSimple[Num ~int | ~float64](val Num) Num {
	if val < 0 {
		return -val
	}
	return val
}

func addAbsoluteSimple[Num ~int | ~float64](a, b Num) Num {
	return absoluteSimple(a) + absoluteSimple(b)
}

//=======================

//=======================  generics 2 : constraint ======

type num interface {
	int | float64
}

func absoluteConstraint[N num](val N) N {
	if val < 0 {
		return -val
	}
	return val
}

func addAbsoluteConstraint[N num](a, b N) N {
	return absoluteConstraint(a) + absoluteConstraint(b)
}

//=======================

//=======================  generics 3 : tilde ~ ======

type num2 interface {
	~int | ~float64
}

type float float64

func absoluteConstraint2[N num2](val N) N {
	if val < 0 {
		return -val
	}
	return val
}

func addAbsoluteConstraintOwnFloat[N num2](a, b N) N {
	return absoluteConstraint2(a) + absoluteConstraint2(b)
}

//=======================

//=======================  generics 4 : many generics ======

func absoluteAsWhatever[Num, Ret num](val Num) Ret {
	if val < 0 {
		return Ret(-val)
	}
	return Ret(val)
}

func addAbsoluteFromWhateverAsFirst[NumA, NumB num](a NumA, b NumB) NumA {
	return absoluteAsWhatever[NumA, NumA](a) + absoluteAsWhatever[NumB, NumA](b)
}

func addAbsoluteFromWhateverAsSecond[NumA, NumB num](a NumA, b NumB) NumB {
	return absoluteAsWhatever[NumA, NumB](a) + absoluteAsWhatever[NumB, NumB](b)
}

func addAbsoluteFromWhateverAsWhatever[NumA, NumB, Ret num](a NumA, b NumB) Ret {
	return absoluteAsWhatever[NumA, Ret](a) + absoluteAsWhatever[NumB, Ret](b)
}

//=======================
